import tkinter as tk
import tkinter.filedialog as filedialog
from PIL import Image, ImageTk
from os.path import splitext

# 畫面配置
window = tk.Tk() # 建立視窗window
window.title('AIP60947047S')# 視窗名稱
window.geometry('1024x768')# 設定視窗的大小(長 * 寬)

# 以下主程式
render = None
filePath = None
subFileName = None

# 按鈕事件
def btnClick():
    global filePath, subFileName
    filePath = filedialog.askopenfilename()
    temp = splitext(filePath)[-1]
    subFileName = temp[1:]
    if (subFileName != ''):
        showImage()

def showImage():
    global render
    img = Image.open(filePath)
    width, height = img.size
    lb.config(text = "原始 " + str(width) + "x" + str(height) + " " + subFileName + " 檔")
    img = img.resize((450, 450), Image.ANTIALIAS)
    render = ImageTk.PhotoImage(img)
    inputImg.configure(image = render)
    outputImg.configure(image = render)

# 物件放置
frame1 = tk.Frame(window, width= 450, height= 600)
inputImg = tk.Label(frame1)
inputImg.pack()
frame2 = tk.Frame(window, width= 450, height = 600)
outputImg = tk.Label(frame2)
outputImg.pack()
frame1.place(x = 0, y = 100)
frame2.place(x = 500, y = 100)

lb = tk.Label(window, text = '', font = 30)
lb.place(x = 450, y = 50)

loadBtn = tk.Button(window, text='上傳檔案', font=('Arial', 12), width=10, height=1, command=btnClick)
loadBtn.pack()

# 主視窗迴圈顯示
window.mainloop()

