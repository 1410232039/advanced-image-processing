import tkinter as tk
import tkinter.filedialog as filedialog
from PIL import Image, ImageTk, ImageDraw
from os.path import splitext
import numpy as np
from random import uniform
from math import sin, cos, sqrt, log

# 畫面配置
window = tk.Tk() # 建立視窗window
window.title('AIP60947047S')# 視窗名稱
window.geometry('1024x768')# 設定視窗的大小(長 * 寬)

# 以下主程式
render = None
filePath = None
subFileName = None
img = None
sigma = 0
pi = 3.14

# 按鈕事件
def btnClick():
    global filePath, subFileName
    filePath = filedialog.askopenfilename()
    temp = splitext(filePath)[-1]
    subFileName = temp[1:]
    if (subFileName != ''):
        global img
        img = Image.open(filePath).convert('F')
        width, height = img.size
        lb.config(text="原始 " + str(width) + "x" + str(height) + " " + subFileName + " 檔")
        img = img.resize((450, 450), Image.ANTIALIAS)
        createImage(img)
        getHist(img)

# 產生圖片
def createImage(img):
    global render
    render = ImageTk.PhotoImage(img)
    inputImg.configure(image = render)

# 產生輸出影像直方圖
def getHist(img):
    global _Image, newImg, outputImg
    inputArr = np.array(img)
    outputArr = np.ndarray.flatten(inputArr)
    outputArr = np.round(outputArr)
    values, count = np.unique(outputArr, return_counts=True)

    width = 450
    height = 450
    newImg = Image.new("RGB", (width, height), (255, 255, 255))
    draw = ImageDraw.Draw(newImg)

    for i, j in zip(values, count):
        j = round(j * 450 / max(count))
        draw.rectangle([(0, i), (j, i)], fill='Red')

    newImg = newImg.rotate(90)
    _Image = ImageTk.PhotoImage(newImg)
    outputImg.configure(image=_Image)

# 產生雜訊
def generate(sigma, temp1, temp2):
    global pi
    r = uniform(0, 1)
    phi = uniform(0, 1)
    z1 = sigma*cos(2*pi*phi)*sqrt((-2)*log(r))
    z2 = sigma*sin(2*pi*phi)*sqrt((-2)*log(r))

    return branch(temp1 + z1), branch(temp2 + z2)

# 數值判斷
def branch(num):
    if num > 255:
        return 255
    elif num < 0:
        return 0
    else:
        return num

# 加入雜訊
def noise():
    global sigma
    gray = img.copy()
    temp = gray.load()
    width, height = gray.size
    f = np.zeros(shape = (height, width))
    for i in range(height):
        for j in range(0, width, 2):
            f[i, j], f[i, j + 1] = generate(sigma, temp[j, i], temp[j + 1, i])

    for i in range(f.shape[0]):
        for j in range(f.shape[1]):
            temp[j, i] = f[i, j]

    createImage(gray)
    getHist(gray)

# 拉條
def selectBar(v):
    global sigma
    l.config(text='σ = ' + v)
    sigma = int(v)

# 輸入輸出放置
frame1 = tk.Frame(window, width= 450, height= 600)
inputImg = tk.Label(frame1)
inputImg.pack()
frame2 = tk.Frame(window, width= 450, height = 600)
outputImg = tk.Label(frame2)
outputImg.pack()
frame1.place(x = 0, y = 100)
frame2.place(x = 500, y = 100)

# 標籤文字
lb = tk.Label(window, text = '', font = 30)
lb.place(x = 450, y = 50)

# 按鈕
loadBtn = tk.Button(window, text = '上傳檔案', font = 65, width = 8, height = 2, command = btnClick)
loadBtn.place(x = 0, y = 0)
l = tk.Label(window, bg='red', width=20, text='高斯雜訊')
l.place(x = 300, y = 0)
s = tk.Scale(window, label='σ number', from_=0, to=40, orient=tk.HORIZONTAL,
showvalue=0, tickinterval=10, resolution=1, length=300, command=selectBar)
s.place(x = 300, y = 30)
btn4 = tk.Button(window, text = "產生雜訊", command = lambda : noise(), font = 65, height = 2, width = 8)
btn4.place(x = 82, y = 0)
# 主視窗迴圈顯示
window.mainloop()

