import tkinter as tk
import tkinter.filedialog as filedialog
from PIL import Image, ImageTk, ImageDraw
from os.path import splitext
import numpy as np

# 畫面配置
window = tk.Tk() # 建立視窗window
window.title('AIP60947047S')# 視窗名稱
window.geometry('1024x768')# 設定視窗的大小(長 * 寬)

# 以下主程式
render = None
filePath = None
subFileName = None
img = None

# 按鈕事件
def btnClick():
    global filePath, subFileName
    filePath = filedialog.askopenfilename()
    temp = splitext(filePath)[-1]
    subFileName = temp[1:]
    if (subFileName != ''):
        showImage()

# 產生輸入影像
def showImage():
    global render, img
    img = Image.open(filePath)
    width, height = img.size
    lb.config(text = "原始 " + str(width) + "x" + str(height) + " " + subFileName + " 檔")
    img = img.resize((450, 450), Image.ANTIALIAS)
    render = ImageTk.PhotoImage(img)
    inputImg.configure(image = render)
    # outputImg.configure(image = render)
    # hist(img)

# 產生輸出影像直方圖
def getHist(img):
    global _Image, newImg, outputImg
    inputArr = np.array(img)
    outputArr = np.ndarray.flatten(inputArr)
    outputArr = np.round(outputArr)
    values, count = np.unique(outputArr, return_counts=True)

    width = 450
    height = 450
    newImg = Image.new("RGB", (width, height), (255, 255, 255))
    draw = ImageDraw.Draw(newImg)

    for i, j in zip(values, count):
        j = round(j * 450 / max(count))
        draw.rectangle([(0, i), (j, i)], fill='Red')

    newImg = newImg.rotate(90)
    _Image = ImageTk.PhotoImage(newImg)
    outputImg.configure(image=_Image)


# 輸入輸出放置
frame1 = tk.Frame(window, width= 450, height= 600)
inputImg = tk.Label(frame1)
inputImg.pack()
frame2 = tk.Frame(window, width= 450, height = 600)
outputImg = tk.Label(frame2)
outputImg.pack()
frame1.place(x = 0, y = 100)
frame2.place(x = 500, y = 100)

# 標籤文字
lb = tk.Label(window, text = '', font = 30)
lb.place(x = 450, y = 50)

# 按鈕
loadBtn = tk.Button(window, text = '上傳檔案', font = 65, width = 8, height = 2, command = btnClick)
loadBtn.place(x = 0, y = 0)
btn3 = tk.Button(window, text = "直方圖", command = lambda : getHist(img), font = 65, height = 2, width = 8)
btn3.place(x = 82, y = 0)

# 主視窗迴圈顯示
window.mainloop()

