import tkinter as tk
import tkinter.filedialog as filedialog
from PIL import Image, ImageTk, ImageDraw
from os.path import splitext
import numpy as np
from random import uniform
from math import sin, cos, sqrt, log

# 畫面配置
window = tk.Tk() # 建立視窗window
window.title('AIP60947047S')# 視窗名稱
window.geometry('1024x768')# 設定視窗的大小(長 * 寬)

# 以下主程式
render = None
filePath = None
subFileName = None
img = None
imgArray = None
sigma = 0
pi = 3.14
temp = []


# 按鈕事件
def btnClick():
    global filePath, subFileName
    filePath = filedialog.askopenfilename()
    temp = splitext(filePath)[-1]
    subFileName = temp[1:]
    if (subFileName != ''):
        global img, imgArray
        img = Image.open(filePath).convert('F')
        width, height = img.size
        lb.config(text="原始 " + str(width) + "x" + str(height) + " " + subFileName + " 檔")
        img = img.resize((512, 512), Image.ANTIALIAS)
        createImage(img)
        imgArray = np.asarray(img).astype('int64')

# 產生圖片
def createImage(img):
    global render
    render = ImageTk.PhotoImage(img)
    inputImg.configure(image = render)

# 產生輸出影像直方圖
def getHist(img):
    global _Image, newImg, outputImg
    inputArr = np.array(img)
    outputArr = np.ndarray.flatten(inputArr)
    outputArr = np.round(outputArr)
    values, count = np.unique(outputArr, return_counts=True)

    width = 450
    height = 450
    newImg = Image.new("RGB", (width, height), (255, 255, 255))
    draw = ImageDraw.Draw(newImg)

    for i, j in zip(values, count):
        j = round(j * 450 / max(count))
        draw.rectangle([(0, i), (j, i)], fill='Red')

    newImg = newImg.rotate(90)
    _Image = ImageTk.PhotoImage(newImg)
    outputImg.configure(image=_Image)

# 產生雜訊
def generate(sigma, temp1, temp2):
    global pi
    r = uniform(0, 1)
    phi = uniform(0, 1)
    z1 = sigma*cos(2*pi*phi)*sqrt((-2)*log(r))
    z2 = sigma*sin(2*pi*phi)*sqrt((-2)*log(r))

    return branch(temp1 + z1), branch(temp2 + z2)

# 數值判斷
def branch(num):
    # if num > 255:
    #     return 255
    # elif num < 0:
    #     return 0
    # else:
        return num


# 加入雜訊
def noise():
    global sigma
    gray = img.copy()
    temp = gray.load()
    width, height = gray.size
    f = np.zeros(shape = (height, width))
    for i in range(height):
        for j in range(0, width, 2):
            f[i, j], f[i, j + 1] = generate(sigma, temp[j, i], temp[j + 1, i])

    for i in range(f.shape[0]):
        for j in range(f.shape[1]):
            temp[j, i] = f[i, j]

    createImage(gray)
    getHist(gray)

# 拉條
def selectBar(v):
    global sigma
    l.config(text='σ = ' + v)
    sigma = int(v)


def test(imgArray):
    global TAT

    Hout = int(imgArray.shape[0] / 2)
    Wout = int(imgArray.shape[1] / 2)
    outArray = np.zeros(shape=(Hout*2, Wout*2))

    for i in range(Hout):
        for j in range(Wout):
            A = imgArray[int(i * 2), int(j * 2)]
            B = imgArray[int(i * 2), int(j * 2 + 1)]
            C = imgArray[int(i * 2 + 1), int(j * 2)]
            D = imgArray[int(i * 2 + 1), int(j * 2 + 1)]
            outArray[i, j] = branch((A+B+C+D)/4
            outArray[i, j+2] = branch((A-B+C-D)/4
            outArray[i+2, j] = branch((A+B-C-D)/4
            outArray[i+2, j+2] = branch((A-B-C+D)/4

    # temp.append((outArray))  # 加在結尾

    # return temp
    tat = Image.fromarray(outArray)
    TAT = ImageTk.PhotoImage(tat)
    outputImg.configure(image=TAT)
    print(outArray)


#wavelet transform
def wavelet(imgArray, iteration):
    global temp, _Image, newImg
    a = conv(imgArray, iteration)
    a.reverse()

    for i in range(iteration):
        b = np.concatenate((a.pop(0), a.pop(0)),axis=1)
        c = np.concatenate((a.pop(0), a.pop(0)),axis=1)
        t = np.concatenate((b, c),axis=0)
        a.insert(0, t)

    finalWT = a[0]
    _Image = Image.fromarray(finalWT)
    newImg = ImageTk.PhotoImage(_Image)
    outputImg.configure(image = newImg)

def conv(image, iteration):
    global temp

    # 右下左下右上左上
    filters = ([1,-1,1,-1], [1,1,-1,-1], [1,-1,1,-1], [1,1,1,1])

    for i in filters:
        a,b,c,d = i[0], i[1], i[2], i[3]
        outHeight = int(image.shape[0]/2)
        outWidth = int(image.shape[1]/2)
        output = np.zeros(shape = (outHeight, outWidth))
        # 一次處理一個區塊
        for i in range(output.shape[0]):
            for j in range(output.shape[1]):
                A = a*image[int(i * 2), int(j * 2)]
                B = b*image[int(i * 2), int(j * 2 + 1)]
                C = c*image[int(i * 2 + 1), int(j * 2)]
                D = d*image[int(i * 2 + 1), int(j * 2 + 1)]
                output[i, j] = (A+B+C+D)/4
        temp.append(normalization(output))

    if iteration == 1:
        return temp
    elif iteration > 1:
        next = temp.pop(len(temp)-1) #移除最後一個
        return conv(next, (iteration-1))

# 正規化
def normalization(region):
    temp = np.zeros(shape = region.shape)
    for i in range(region.shape[0]):
        for j in range(region.shape[1]):
            if region[i, j] > 0:
                temp[i,j] = 256*(region[i,j]- 0)/(np.max(region) - 0)

    return temp

def welClick():
    global num
    num = 0
    if e.get():
        if int(e.get()) <= 0:
            outputImg.configure(image = render)
        elif int(e.get()) > 9:
            num = 9
        else:
            num = int(e.get())
        if num != 0:
            wavelet(imgArray, num)


# 輸入輸出放置
frame1 = tk.Frame(window, width= 450, height= 600)
inputImg = tk.Label(frame1)
inputImg.pack()
frame2 = tk.Frame(window, width= 450, height = 600)
outputImg = tk.Label(frame2)
outputImg.pack()
frame1.place(x = 0, y = 100)
frame2.place(x = 500, y = 100)


e = tk.Entry(window)
e.place(x = 170, y = 0)

lb = tk.Label(window, text = '', font = 30)
lb.place(x = 450, y = 50)

# 按鈕
loadBtn = tk.Button(window, text = '上傳檔案', font = 65, width = 8, height = 2, command = btnClick)
loadBtn.place(x = 0, y = 0)
btn4 = tk.Button(window, text = "小波轉換", command = welClick,width=20,height=1)
btn4.place(x = 170, y = 25)
# 主視窗迴圈顯示
window.mainloop()

