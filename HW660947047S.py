import tkinter as tk
import tkinter.filedialog as filedialog
from PIL import Image, ImageTk, ImageDraw
from os.path import splitext
import numpy as np
from random import uniform
from math import sin, cos, sqrt, log

# 畫面配置
window = tk.Tk() # 建立視窗window
window.title('AIP60947047S')# 視窗名稱
window.geometry('1024x768')# 設定視窗的大小(長 * 寬)

# 以下主程式
render = None
filePath = None
subFileName = None
img = None
Hc = None
sigma = 0
pi = 3.14
inputArr = None

# 按鈕事件
def btnClick():
    global filePath, subFileName, img
    filePath = filedialog.askopenfilename()
    temp = splitext(filePath)[-1]
    subFileName = temp[1:]
    if (subFileName != ''):
        global img
        img = Image.open(filePath).convert('L')
        width, height = img.size
        lb.config(text="原始 " + str(width) + "x" + str(height) + " " + subFileName + " 檔")
        img = img.resize((300, 300), Image.ANTIALIAS)
        createImage(img)
        # getHist(img, 1)

# 產生圖片
def createImage(img):
    global render
    render = ImageTk.PhotoImage(img)
    inputImg.configure(image = render)


# 產生輸出影像直方圖
def getHist(img, mode):
    global inputArr, values, count
    inputArr = np.array(img)
    outputArr = np.ndarray.flatten(inputArr)
    outputArr = np.round(outputArr)
    values, count = np.unique(outputArr, return_counts=True)

    width = 300
    height = 300
    newImg = Image.new("RGB", (width, height), (255, 255, 255))
    draw = ImageDraw.Draw(newImg)

    for i, j in zip(values, count):
        j = round(j * 400 / max(count))
        draw.rectangle([(0, i), (j, i)], fill='Red')

    newImg = newImg.rotate(90)

    if mode == 1:
        pict1(newImg)
    elif mode == 2:
        pict2(newImg)



    # equalization(inputArr, values, count)
    # test(inputArr, values, count)


def pict1(newImg):
    global _Image, outputImg
    _Image = ImageTk.PhotoImage(newImg)
    outputImg.configure(image=_Image)

def pict2(newImg):
    global _Image2, outputImg2
    _Image2 = ImageTk.PhotoImage(newImg)
    outputImg2.configure(image=_Image2)

# 產生雜訊
def generate(sigma, temp1, temp2):
    global pi
    r = uniform(0, 1)
    phi = uniform(0, 1)
    z1 = sigma*cos(2*pi*phi)*sqrt((-2)*log(r))
    z2 = sigma*sin(2*pi*phi)*sqrt((-2)*log(r))

    return branch(temp1 + z1), branch(temp2 + z2)

# 數值判斷
def branch(num):
    if num > 255:
        return 255
    elif num < 0:
        return 0
    else:
        return num

# 加入雜訊
def noise():
    global sigma
    gray = img.copy()
    temp = gray.load()
    width, height = gray.size
    f = np.zeros(shape = (height, width))
    for i in range(height):
        for j in range(0, width, 2):
            f[i, j], f[i, j + 1] = generate(sigma, temp[j, i], temp[j + 1, i])

    for i in range(f.shape[0]):
        for j in range(f.shape[1]):
            temp[j, i] = f[i, j]

    createImage(gray)
    getHist(gray)

# 拉條
def selectBar(v):
    global sigma
    l.config(text='σ = ' + v)
    sigma = int(v)

def Transform(intensity, Hc, values):
    g = intensity
    hcValue = Hc[np.where(values == round(g))[0][0]]
    hMin = Hc[0]
    mn = Hc[-1]
    return round((hcValue - hMin) / (mn - hMin) * 255)

def viewMode2(avatar):
    global _Image3, inputImg2
    _Image3 = ImageTk.PhotoImage(avatar)
    inputImg2.configure(image=_Image3)

def equalization(inputArr, countH, values):
    global Hc
    lenCountH = len(countH)
    Hc = [None] * lenCountH
    Hc[0] = countH[0]

    for i in range(1, lenCountH):
        Hc[i] = Hc[i-1] + countH[i]

    output = np.zeros(shape=inputArr.shape)

    for i in range(inputArr.shape[0]):
        for j in range(inputArr.shape[1]):
            output[i, j] = Transform(inputArr[i, j], Hc, values)

    output1 = Image.fromarray(output)

    viewMode2(output1)

    getHist(output, 2)

def gaussian_smooth(size):
    x, y = np.mgrid[-(size-2):(size-1), -(size-2):(size-1)]
    gaussian_kernel = np.multiply(1/(2*pi*(9**2)), np.exp(-(x**2+y**2)/(2*(9**2))))
    gaussian_kernel = gaussian_kernel / gaussian_kernel.sum()
    return gaussian_kernel

def edge_detection(img, size):
    img = np.array(img)
    blur = convolve(img, gaussian_smooth(size))
    x, y = xyfilter(size)
    a = convolve(blur, x)
    b = convolve(blur, y)
    c = np.sqrt(np.add(np.square(a), np.square(b)))
    c = c / c.max() * 255
    pict1(Image.fromarray(c))

def smoothing(img, size):
    img = np.array(img)
    a = gaussian_smooth(size)
    b = convolve(img, a)
    pict1(Image.fromarray(b))

def xyfilter(size):
    x , y = np.mgrid[-(size-2):(size-1), -(size-2):(size-1)]
    return x, y

def convolve(img, kernel):
    """Performs a naive convolution."""
    if kernel.shape[0] % 2 != 1 or kernel.shape[1] % 2 != 1:
        raise ValueError("Only odd dimensions on filter supported")

    img_height = img.shape[0]
    img_width = img.shape[1]
    pad_height = kernel.shape[0] // 2
    pad_width = kernel.shape[1] // 2
    # Allocate result image.
    pad = ((pad_height, pad_height), (pad_height, pad_width))
    g = np.empty(img.shape, dtype=np.float64)
    img = np.pad(img, pad, mode='constant', constant_values=0)
    # Do convolution
    for i in np.arange(pad_height, img_height+pad_height):
        for j in np.arange(pad_width, img_width+pad_width):
            roi = img[i - pad_height:i + pad_height +
                      1, j - pad_width:j + pad_width + 1]
            g[i - pad_height, j - pad_width] = (roi*kernel).sum()

    if (g.dtype == np.float64):
        kernel = kernel / 255.0
        kernel = (kernel*255).astype(np.uint8)
    else:
        g = g + abs(np.amin(g))
        g = g / np.amax(g)
        g = (g*255.0)

    return g

# 輸入輸出放置
frame1 = tk.Frame(window, width= 500, height= 500)
inputImg = tk.Label(frame1)
inputImg.pack()
frame2 = tk.Frame(window, width= 500, height = 500)
outputImg = tk.Label(frame2)
outputImg.pack()
frame1.place(x = 0, y = 50)
frame2.place(x = 650, y = 50)


e = tk.Entry(window)
e.place(x = 80, y = 40)
eb = tk.Label(window, text = 'Conv Size', font = 20).place(x = 140, y = 68)
def insert_point(instruction):
    global num
    num = 0
    if e.get():
        g = int(e.get())
        if (g - 1)%2 == 1:
            outputImg.configure(image = render)
        elif instruction == 1:
            num = g
            smoothing(img, num)
        elif instruction == 0:
            num = g
            edge_detection(img, num)

# 標籤文字
lb = tk.Label(window, text = '', font = 30)
lb.place(x = 470, y = 50)

# 按鈕
loadBtn = tk.Button(window, text = '上傳檔案', font = 65, width = 8, height = 2, command = btnClick)
loadBtn.place(x = 0, y = 0)
b1 = tk.Button(window,text="邊緣偵測",width=20,height=1,command=lambda:insert_point(0))
b1.place(x = 80, y = 0)
b2 = tk.Button(window,text="影像平滑化",width=20,height=1,command=lambda:insert_point(1))
b2.place(x = 80, y = 20)
# btn4 = tk.Button(window, text = "直方圖均化", command = lambda : equalization(inputArr, count, values), font = 65, height = 2, width = 8)
# btn4.place(x = 82, y = 0)
# 主視窗迴圈顯示
window.mainloop()

