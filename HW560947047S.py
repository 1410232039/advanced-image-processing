import tkinter as tk
import tkinter.filedialog as filedialog
from PIL import Image, ImageTk, ImageDraw
from os.path import splitext
import numpy as np
from random import uniform
from math import sin, cos, sqrt, log

# 畫面配置
window = tk.Tk() # 建立視窗window
window.title('AIP60947047S')# 視窗名稱
window.geometry('1024x768')# 設定視窗的大小(長 * 寬)

# 以下主程式
render = None
filePath = None
subFileName = None
img = None
Hc = None
sigma = 0
pi = 3.14
inputArr = None

# 按鈕事件
def btnClick():
    global filePath, subFileName, img
    filePath = filedialog.askopenfilename()
    temp = splitext(filePath)[-1]
    subFileName = temp[1:]
    if (subFileName != ''):
        global img
        img = Image.open(filePath).convert('L')
        width, height = img.size
        lb.config(text="原始 " + str(width) + "x" + str(height) + " " + subFileName + " 檔")
        img = img.resize((300, 300), Image.ANTIALIAS)
        createImage(img)
        getHist(img, 1)

# 產生圖片
def createImage(img):
    global render
    render = ImageTk.PhotoImage(img)
    inputImg.configure(image = render)


# 產生輸出影像直方圖
def getHist(img, mode):
    global inputArr, values, count
    inputArr = np.array(img)
    outputArr = np.ndarray.flatten(inputArr)
    outputArr = np.round(outputArr)
    values, count = np.unique(outputArr, return_counts=True)

    width = 300
    height = 300
    newImg = Image.new("RGB", (width, height), (255, 255, 255))
    draw = ImageDraw.Draw(newImg)

    for i, j in zip(values, count):
        j = round(j * 400 / max(count))
        draw.rectangle([(0, i), (j, i)], fill='Red')

    newImg = newImg.rotate(90)

    if mode == 1:
        pict1(newImg)
    elif mode == 2:
        pict2(newImg)



    # equalization(inputArr, values, count)
    # test(inputArr, values, count)


def pict1(newImg):
    global _Image, outputImg
    _Image = ImageTk.PhotoImage(newImg)
    outputImg.configure(image=_Image)

def pict2(newImg):
    global _Image2, outputImg2
    _Image2 = ImageTk.PhotoImage(newImg)
    outputImg2.configure(image=_Image2)

# 產生雜訊
def generate(sigma, temp1, temp2):
    global pi
    r = uniform(0, 1)
    phi = uniform(0, 1)
    z1 = sigma*cos(2*pi*phi)*sqrt((-2)*log(r))
    z2 = sigma*sin(2*pi*phi)*sqrt((-2)*log(r))

    return branch(temp1 + z1), branch(temp2 + z2)

# 數值判斷
def branch(num):
    if num > 255:
        return 255
    elif num < 0:
        return 0
    else:
        return num

# 加入雜訊
def noise():
    global sigma
    gray = img.copy()
    temp = gray.load()
    width, height = gray.size
    f = np.zeros(shape = (height, width))
    for i in range(height):
        for j in range(0, width, 2):
            f[i, j], f[i, j + 1] = generate(sigma, temp[j, i], temp[j + 1, i])

    for i in range(f.shape[0]):
        for j in range(f.shape[1]):
            temp[j, i] = f[i, j]

    createImage(gray)
    getHist(gray)

# 拉條
def selectBar(v):
    global sigma
    l.config(text='σ = ' + v)
    sigma = int(v)

def Transform(intensity, Hc, values):
    g = intensity
    hcValue = Hc[np.where(values == round(g))[0][0]]
    hMin = Hc[0]
    mn = Hc[-1]
    return round((hcValue - hMin) / (mn - hMin) * 255)

def viewMode2(avatar):
    global _Image3, inputImg2
    _Image3 = ImageTk.PhotoImage(avatar)
    inputImg2.configure(image=_Image3)

def equalization(inputArr, countH, values):
    global Hc
    lenCountH = len(countH)
    Hc = [None] * lenCountH
    Hc[0] = countH[0]

    for i in range(1, lenCountH):
        Hc[i] = Hc[i-1] + countH[i]

    output = np.zeros(shape=inputArr.shape)

    for i in range(inputArr.shape[0]):
        for j in range(inputArr.shape[1]):
            output[i, j] = Transform(inputArr[i, j], Hc, values)

    output1 = Image.fromarray(output)

    viewMode2(output1)

    getHist(output, 2)

def testF(intensity, Hc, hmin):
    g = intensity
    hcValue = Hc[g]
    hMin = hmin
    mn = Hc[-1]
    return round((hcValue - hMin) / (mn - hMin) * 255)


def test(inputArr, values, countH):
    global Hc
    lenCountH = len(countH)
    Hc = [None] * lenCountH
    Hc[0] = countH[0]

    for i in range(1, lenCountH):
        Hc[i] = Hc[i-1] + countH[i]

    output = np.zeros(shape=inputArr.shape)
    #


    test = [0]*256
    for i in range(inputArr.shape[0]):
        for j in range(inputArr.shape[1]):
            value = inputArr[i, j]
            test[value]+=1


    test = np.array(test)
    test.sort()
    for i in range(len(test)):
        if(test[i]) > 0:
            gmin = i
            break

    hmin = Hc[gmin]

    for i in range(inputArr.shape[0]):
        for j in range(inputArr.shape[1]):
            output[i, j] = testF(inputArr[i, j], Hc, hmin)







# 輸入輸出放置
frame1 = tk.Frame(window, width= 300, height= 300)
inputImg = tk.Label(frame1)
inputImg.pack()
frame2 = tk.Frame(window, width= 300, height = 300)
outputImg = tk.Label(frame2)
outputImg.pack()
frame1.place(x = 0, y = 50)
frame2.place(x = 650, y = 50)
frame3 = tk.Frame(window, width = 300, height = 300)
inputImg2 = tk.Label(frame3)
inputImg2.pack()
frame4 = tk.Frame(window, width = 300, height = 300)
outputImg2 = tk.Label(frame4)
outputImg2.pack()
frame3.place(x = 0, y = 350)
frame4.place(x = 650, y = 350)
# 標籤文字
lb = tk.Label(window, text = '', font = 30)
lb.place(x = 470, y = 50)

# 按鈕
loadBtn = tk.Button(window, text = '上傳檔案', font = 65, width = 8, height = 2, command = btnClick)
loadBtn.place(x = 0, y = 0)
# btn2 = tk.Button(window, text = "直方圖均化", command = lambda : equalization(inputArr, count, values), font = 80, height = 3, width = 10)
# btn2.place(x = 140, y = 0)
# l = tk.Label(window, bg='red', width=20, text='高斯雜訊')
# l.place(x = 300, y = 0)
# s = tk.Scale(window, label='σ number', from_=0, to=40, orient=tk.HORIZONTAL,
# showvalue=0, tickinterval=10, resolution=1, length=300, command=selectBar)
# s.place(x = 300, y = 30)
btn4 = tk.Button(window, text = "直方圖均化", command = lambda : equalization(inputArr, count, values), font = 65, height = 2, width = 8)
btn4.place(x = 82, y = 0)
# 主視窗迴圈顯示
window.mainloop()

